# CPU Scheduling Algorithms Simulator

A C++ library for simulating CPU scheduling algorithms.

## Algorithms

* First Come First Serve [FCFS]
* Shortest Job First (Non-preemptive/Preemptive) [NPSJF/PSJF]
* Priority (Non-preemptive/Preemptive) [NPP/PP]
* Round Robin [RR]

## License

This project is licensed under the GNU GPL v3 - see [LICENSE.md](LICENSE.md) for details.

## Usage

Create an instance.
```c++
CSAS::Process processes[] =
{
    {1, 0, 6, 1},
    {2, 0, 8, 2},
    {3, 0, 7, 3},
    {4, 0, 3, 4}
};

unsigned int processCount = sizeof(processes) / sizeof(processes[0]);

CSAS csas(processes, processCount, CSAS::FCFS);
```

Loop step simulation.
```c++
bool done = false;

while (done == false)
{
    csas.updateQueue();
	
    CSAS::StagedProcess* const * const queue = csas.getStagedProcessQueue();
    unsigned int count = csas.getInQueueCount();
    
    std::cout << "Queue: |";
    for (unsigned int i = 0 ; i < count ; i++)
    std::cout << " " << queue[i]->id << " (" << queue[i]->duration - queue[i]->count << ") |";
    
    std::cout << std::endl;
    
    const CSAS::Step* currentStep = csas.getRootStep();
    
    std::cout << "Bar Graph: |";
    while (currentStep != nullptr)
    {
        std::cout << " " << currentStep->id << " |";
        currentStep = currentStep->next;
    }
    
    std::cout << std::endl << std::endl;
    
    done = !csas.simulateSingleStep();
}
```

Finalize.
```c++
csas.updateQueue();
csas.finalize();

CSAS::StagedProcess* const * const queue = csas.getStagedProcessQueue();
unsigned int count = csas.getInQueueCount();

std::cout << "Queue: |";
for (unsigned int i = 0 ; i < count ; i++)
    std::cout << " " << queue[i]->id << " (" << queue[i]->duration - queue[i]->count << ") |";

std::cout << std::endl;

const CSAS::Step* currentStep = csas.getRootStep();

std::cout << "Bar Graph: |";
while (currentStep != nullptr)
{
    std::cout << " " << currentStep->id << " |";
    currentStep = currentStep->next;
}

std::cout << std::endl << std::endl;

const CSAS::StagedProcess* const * const stagedProcesses = csas.getStagedProcesses();

for (unsigned int i = 0 ; i < count ; i++)
    std::cout << "ID: " << stagedProcesses[i]->id
        << " Completion: " << stagedProcesses[i]->completion
        << " Turn-Around: " << stagedProcesses[i]->turnaround
        << " Wait: " << stagedProcesses[i]->wait << std::endl;

std::cout << "\nTotal Wait: " << csas.getTotalWait() << std::endl
    << "Average Wait: " << csas.getAverageWait() << std::endl
    << "Total Turnaround: " << csas.getTotalTurnaround() << std::endl
    << "Average Turnaround: " << csas.getAverageTurnaround() << std::endl;

std::cout << std::endl;
```

### Alternative

Simulate all.
```c++
csas.simulateAllSteps();
csas.finalize();

CSAS::StagedProcess* const * const queue = csas.getStagedProcessQueue();
unsigned int count = csas.getInQueueCount();

std::cout << "Queue: |";
for (unsigned int i = 0 ; i < count ; i++)
    std::cout << " " << queue[i]->id << " (" << queue[i]->duration - queue[i]->count << ") |";

std::cout << std::endl;

const CSAS::Step* currentStep = csas.getRootStep();

std::cout << "Bar Graph: |";
while (currentStep != nullptr)
{
    std::cout << " " << currentStep->id << " |";
    currentStep = currentStep->next;
}

std::cout << std::endl << std::endl;

const CSAS::StagedProcess* const * const stagedProcesses = csas.getStagedProcesses();

for (unsigned int i = 0 ; i < count ; i++)
    std::cout << "ID: " << stagedProcesses[i]->id
        << " Completion: " << stagedProcesses[i]->completion
        << " Turn-Around: " << stagedProcesses[i]->turnaround
        << " Wait: " << stagedProcesses[i]->wait << std::endl;

std::cout << "\nTotal Wait: " << csas.getTotalWait() << std::endl
    << "Average Wait: " << csas.getAverageWait() << std::endl
    << "Total Turnaround: " << csas.getTotalTurnaround() << std::endl
    << "Average Turnaround: " << csas.getAverageTurnaround() << std::endl;

std::cout << std::endl;
```