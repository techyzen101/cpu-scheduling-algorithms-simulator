/*
*	This file is part of CPU Scheduling Algorithms Simulator (CSAS) library.
*	Copyright (C) 2019 by Lester Espiritu (techyzen101@gmail.com)
*
*	CPU Scheduling Algorithms Simulator (CSAS) library is free software:
*	you can redistribute it and/or modify it under the terms of the
*	GNU General Public License as published by the Free Software Foundation,
*	either version 3 of the License, or any later version.
*
*	CPU Scheduling Algorithms Simulator (CSAS) library is distributed
*	in the hope that it will be useful, but WITHOUT ANY WARRANTY;
*	without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
*	for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with CPU Scheduling Algorithms Simulator (CSAS) library.
*	If not, see <https://www.gnu.org/licenses/>.
*/

#include "CSAS.hpp"

namespace
{
	CSAS::StagedProcess** StageProcesses(const CSAS::Process*, const unsigned int&);
	void UnstageProcesses(CSAS::StagedProcess**, const unsigned int&);
	
	void CleanQueue(CSAS::StagedProcess**, unsigned int&);
	void Swap(CSAS::StagedProcess**, CSAS::StagedProcess**);
	void SortQueue(CSAS::StagedProcess**, const unsigned int&);
	
	void AddStep(const unsigned int&, CSAS::Step**, CSAS::Step**);
	void CleanSteps(CSAS::Step**, CSAS::Step**);
}

CSAS::CSAS(const Process* processes, const unsigned int& processCount, const Algorithm& algorithm, const unsigned int& quantum) :
algorithm(algorithm), quantum(quantum), stagedProcessCount(processCount)
{
	stagedProcesses = StageProcesses(processes, processCount);
	stagedProcessQueue = new StagedProcess*[processCount];
	
	firstStep = nullptr;
	lastStep = nullptr;
	
	time = 0;
	pickedQueueIndex = 0;
	pickedAttribute = 0;
	
	inQueueCount = 0;
	queuedCount = 0;
	
	started = false;
	
	totalWait = 0;
	totalTurnaround = 0;
}

CSAS::~CSAS()
{
	UnstageProcesses(stagedProcesses, stagedProcessCount);
	delete[] stagedProcessQueue;
	CleanSteps(&firstStep, &lastStep);
}

void CSAS::updateQueue()
{
	for (unsigned int i = 0 ; i < stagedProcessCount ; i++)
	{
		if (stagedProcesses[i]->arrival <= time && stagedProcesses[i]->queued == false)
		{
			stagedProcessQueue[inQueueCount] = stagedProcesses[i];
			stagedProcesses[i]->queued = true;
			inQueueCount++;
			queuedCount++;
		}
	}
}

bool CSAS::simulateSingleStep()
{
	if (inQueueCount > 0)
	{	
		if (algorithm == NPSJF)
		{
			pickedAttribute = stagedProcessQueue[pickedQueueIndex]->duration;
		}
		else if (algorithm == PSJF)
		{
			pickedQueueIndex = 0;
			pickedAttribute = stagedProcessQueue[pickedQueueIndex]->duration - stagedProcessQueue[pickedQueueIndex]->count;
		}
		else if (algorithm == NPP)
		{
			pickedAttribute = stagedProcessQueue[pickedQueueIndex]->priority;
		}
		else if (algorithm == PP)
		{
			pickedQueueIndex = 0;
			pickedAttribute = stagedProcessQueue[pickedQueueIndex]->priority;
		}
		
		if (algorithm != FCFS)
		{
			for (unsigned int i = 1 ; i < inQueueCount ; i++)
			{
				if (algorithm == NPSJF)
				{
					if (started == true)
						break;
					
					unsigned int duration = stagedProcessQueue[i]->duration;
					
					if (duration < pickedAttribute)
					{
						pickedAttribute = duration;
						pickedQueueIndex = i;
					}
				}
				else if (algorithm == PSJF)
				{
					unsigned int delta = stagedProcessQueue[i]->duration - stagedProcessQueue[i]->count;
					
					if (delta > 0 && delta < pickedAttribute)
					{
						pickedAttribute = delta;
						pickedQueueIndex = i;
					}
				}
				else if (algorithm == NPP)
				{
					if (started == true)
						break;
					
					unsigned int priority = stagedProcessQueue[i]->priority;
					
					if (priority < pickedAttribute)
					{
						pickedAttribute = priority;
						pickedQueueIndex = i;
					}
				}
				else if (algorithm == PP)
				{
					unsigned int priority = stagedProcessQueue[i]->priority;
					
					if (priority < pickedAttribute)
					{
						pickedAttribute = priority;
						pickedQueueIndex = i;
					}
				}
			}
		}
		else
			pickedQueueIndex = 0;
		
		stagedProcessQueue[pickedQueueIndex]->count++;
		AddStep(stagedProcessQueue[pickedQueueIndex]->id, &firstStep, &lastStep);
		
		bool processFinished = stagedProcessQueue[pickedQueueIndex]->duration == stagedProcessQueue[pickedQueueIndex]->count;
		
		if (algorithm == NPSJF || algorithm == NPP)
		{
			if (processFinished)
			{
				unsigned int completion = time + 1;
				unsigned int turnaround = completion - stagedProcessQueue[pickedQueueIndex]->arrival;
				unsigned int wait = turnaround - stagedProcessQueue[pickedQueueIndex]->duration;
				
				stagedProcessQueue[pickedQueueIndex]->completion = completion;
				stagedProcessQueue[pickedQueueIndex]->turnaround = turnaround;
				stagedProcessQueue[pickedQueueIndex]->wait = wait;
				
				totalWait += wait;
				totalTurnaround += turnaround;
				
				pickedQueueIndex = 0;
				started = false;
			}
			else if (started == false)
			{
				stagedProcessQueue[pickedQueueIndex]->completion = time + 1;
				started = true;
			}
		}
		else if (algorithm == RR)
		{
			if (processFinished)
			{
				unsigned int completion = time + 1;
				unsigned int turnaround = completion - stagedProcessQueue[pickedQueueIndex]->arrival;
				unsigned int wait = turnaround - stagedProcessQueue[pickedQueueIndex]->duration;
				
				stagedProcessQueue[pickedQueueIndex]->completion = completion;
				stagedProcessQueue[pickedQueueIndex]->turnaround = turnaround;
				stagedProcessQueue[pickedQueueIndex]->wait = wait;
				
				totalWait += wait;
				totalTurnaround += turnaround;
				
				pickedQueueIndex = 0;
			}
			else if ((stagedProcessQueue[pickedQueueIndex]->count % quantum) == 0)
				pickedQueueIndex = (pickedQueueIndex + 1) % inQueueCount;
		}
		else
		{
			if (processFinished)
			{
				unsigned int completion = time + 1;
				unsigned int turnaround = completion - stagedProcessQueue[pickedQueueIndex]->arrival;
				unsigned int wait = turnaround - stagedProcessQueue[pickedQueueIndex]->duration;
				
				stagedProcessQueue[pickedQueueIndex]->completion = completion;
				stagedProcessQueue[pickedQueueIndex]->turnaround = turnaround;
				stagedProcessQueue[pickedQueueIndex]->wait = wait;
				
				totalWait += wait;
				totalTurnaround += turnaround;
			}
		}
		
		CleanQueue(stagedProcessQueue, inQueueCount);
	}
	else
		AddStep(0, &firstStep, &lastStep);
	
	time++;
	
	if (queuedCount == stagedProcessCount && inQueueCount == 0)
		return false;
	
	return true;
}

void CSAS::simulateAllSteps()
{
	bool done = false;
	
	while (done == false)
	{
		updateQueue();
		done = !simulateSingleStep();
	}
	
	updateQueue();
}

void CSAS::finalize()
{
	averageTurnaround = static_cast<double>(totalTurnaround) / static_cast<double>(stagedProcessCount);
	averageWait = static_cast<double>(totalWait) / static_cast<double>(stagedProcessCount);
}

CSAS::StagedProcess* const * const CSAS::getStagedProcessQueue() const
{
	return stagedProcessQueue;
}

unsigned int CSAS::getInQueueCount() const
{
	return inQueueCount;
}

const CSAS::Step* CSAS::getRootStep() const
{
	return firstStep;
}

unsigned int CSAS::getSimulationTime() const
{
	return time;
}

const CSAS::StagedProcess* const * const CSAS::getStagedProcesses() const
{
	return stagedProcesses;
}

unsigned int CSAS::getTotalWait() const
{
	return totalWait;
}

double CSAS::getAverageWait() const
{
	return averageWait;
}

unsigned int CSAS::getTotalTurnaround() const
{
	return totalTurnaround;
}

double CSAS::getAverageTurnaround() const
{
	return averageTurnaround;
}

namespace
{
	CSAS::StagedProcess** StageProcesses(const CSAS::Process* processes, const unsigned int& processCount)
	{
		CSAS::StagedProcess** stagedProcesses = new CSAS::StagedProcess*[processCount];
		
		for (unsigned int i = 0 ; i < processCount ; i++)
		{
			CSAS::StagedProcess* sp = new CSAS::StagedProcess;
			sp->id = processes[i].id;
			sp->arrival = processes[i].arrival;
			sp->duration = processes[i].duration;
			sp->priority = processes[i].priority;
			sp->count = 0;
			sp->completion = 0;
			sp->wait = 0;
			sp->turnaround = 0;
			sp->queued = false;
			
			stagedProcesses[i] = sp;
		}
		
		return stagedProcesses;
	}
	
	void UnstageProcesses(CSAS::StagedProcess** stagedProcesses, const unsigned int& processCount)
	{
		for (unsigned int i = 0 ; i < processCount ; i++)
			delete stagedProcesses[i];
		
		delete[] stagedProcesses;
	}
	
	void CleanQueue(CSAS::StagedProcess** pProcessQueue, unsigned int& processInQueueCount)
	{
		unsigned int cleanedCount = 0;
		
		for (unsigned int i = 0 ; i < processInQueueCount ; i++)
		{
			if (pProcessQueue[i]->duration == pProcessQueue[i]->count)
			{
				pProcessQueue[i] = nullptr;
				cleanedCount++;
			}
		}
		
		SortQueue(pProcessQueue, processInQueueCount);
		
		processInQueueCount -= cleanedCount;
	}

	void Swap(CSAS::StagedProcess** pProcessA, CSAS::StagedProcess** pProcessB)
	{
		CSAS::StagedProcess* temp = *pProcessA;
		*pProcessA = *pProcessB;
		*pProcessB = temp;
	}

	void SortQueue(CSAS::StagedProcess** pProcesses, const unsigned int& processCount)
	{
		bool swap;
		for (unsigned int i = 0 ; i < processCount - 1 ; i++)
		{
			swap = false;
			for (unsigned int j = 0 ; j < processCount - i - 1 ; j++)
			{	
				if (pProcesses[j] == nullptr)
				{
					swap = true;
					Swap(&pProcesses[j], &pProcesses[j + 1]);
				}
			}
			
			if (swap == false)
				break;
		}
	}
	
	void AddStep(const unsigned int& id, CSAS::Step** pFirstStep, CSAS::Step** pLastStep)
	{
		if (*pFirstStep == nullptr)
		{
			*pFirstStep = new CSAS::Step;
			(*pFirstStep)->id = id;
			(*pFirstStep)->next = nullptr;
			*pLastStep = *pFirstStep;
		}
		else
		{
			CSAS::Step* newStep = new CSAS::Step;
			newStep->id = id;
			newStep->next = nullptr;
			(*pLastStep)->next = newStep;
			*pLastStep = newStep;
		}
	}
	
	void CleanSteps(CSAS::Step** pFirstStep, CSAS::Step** pLastStep)
	{
		CSAS::Step* currentStep = *pFirstStep;
		
		while (currentStep != nullptr)
		{
			CSAS::Step* oldStep = currentStep;
			currentStep = currentStep->next;
			delete oldStep;
		}
		
		*pFirstStep = nullptr;
		*pLastStep = nullptr;
	}
}