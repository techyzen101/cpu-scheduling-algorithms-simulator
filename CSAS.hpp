/*
*	This file is part of CPU Scheduling Algorithms Simulator (CSAS) library.
*	Copyright (C) 2019 by Lester Espiritu (techyzen101@gmail.com)
*
*	CPU Scheduling Algorithms Simulator (CSAS) library is free software:
*	you can redistribute it and/or modify it under the terms of the
*	GNU General Public License as published by the Free Software Foundation,
*	either version 3 of the License, or any later version.
*
*	CPU Scheduling Algorithms Simulator (CSAS) library is distributed
*	in the hope that it will be useful, but WITHOUT ANY WARRANTY;
*	without even the implied warranty of MERCHANTABILITY or
*	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
*	for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with CPU Scheduling Algorithms Simulator (CSAS) library.
*	If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

class CSAS
{
	public:
		struct Process
		{
			unsigned int id, arrival, duration, priority;
		};
		
		struct StagedProcess
		{
			unsigned int id, arrival, duration, priority, count, completion, wait, turnaround;
			bool queued;
		};
		
		struct Step
		{
			unsigned int id;
			Step* next;
		};
		
		enum Algorithm
		{
			FCFS,
			NPSJF,
			PSJF,
			NPP,
			PP,
			RR
		};
		
		CSAS(const Process*, const unsigned int&, const Algorithm&, const unsigned int& = 1);
		~CSAS();
		
		void updateQueue();
		bool simulateSingleStep();
		void finalize();
		
		void simulateAllSteps();
		
		StagedProcess* const * const getStagedProcessQueue() const;
		unsigned int getInQueueCount() const;
		
		const Step* getRootStep() const;
		
		unsigned int getSimulationTime() const;
		
		const StagedProcess* const * const getStagedProcesses() const;
		
		unsigned int getTotalWait() const;
		double getAverageWait() const;
		
		unsigned int getTotalTurnaround() const;
		double getAverageTurnaround() const;
		
	private:
		const Algorithm algorithm;
		const unsigned int quantum;
		
		StagedProcess** stagedProcesses;
		const unsigned int stagedProcessCount;
		
		StagedProcess** stagedProcessQueue;
		unsigned int inQueueCount;
		
		Step* firstStep, *lastStep;
		
		unsigned int time;
		unsigned int queuedCount;
		
		unsigned int pickedQueueIndex;
		unsigned int pickedAttribute;
		
		bool done;
		bool started;
		
		double totalWait, averageWait, totalTurnaround, averageTurnaround;
};